import express from 'express';
import cors from 'cors';

const app = express();
const PORT = 8000;

app.use(cors());

app.get('/getRandomNumber', (req, res) => {
  const lengthOfArray = parseInt(req.query.length as string) || 48;
  if (lengthOfArray > 100) {
    return res.status(400).json({
      status: false,
      message: "length of random number array couldn't be more than 100"
    });
  }
  const randomNumberArray: number[] = [];

  for (var i = 0; i < lengthOfArray; i++) {
    randomNumberArray[i] = i + 1;
  }

  // Randomize the array
  randomNumberArray.sort(function () {
    return Math.random() - 0.5;
  });

  return res.json({ status: true, randomNumbers: randomNumberArray });
});

app.listen(PORT, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${PORT}`);
});
