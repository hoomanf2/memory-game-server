# Memory Game Server

Clone the repo and install the dependencies.

```
git clone https://gitlab.com/hoomanf2/memory-game-server.git
cd memory-game-server
npm i
```

Build and run it:

```
npm run build
npm start
```

Or you can run it in development mode:

```
npm run dev
```
